from flask import Flask, jsonify
from flask_cors import CORS
from Classes.Tools import Tools
from API.SmsApi import SMS
from API.EmailApi import Email_Api
from Database.DB import mongodb

Result = Tools.Result
Error = Tools.errors

app = Flask(__name__)

cors = CORS(app)

# blueprint Register ===
app.register_blueprint(SMS)
app.register_blueprint(Email_Api)


@app.route('/')
def waht():
    return jsonify({"What": "MicroServiceManagement",
                    "Author": "AminJamal",
                    "NickName": "Includeamin",
                    "Email": "aminjamal10@gmail.com",
                    "WebSite": "includeamin.com"})


@app.route('/healthz')
def check_liveness():
    return 'd'


if __name__ == '__main__':
    app.run(port=3008, debug=True, host='0.0.0.0')
