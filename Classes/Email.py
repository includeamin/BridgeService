from datetime import datetime
from Classes.Tools import Tools
from Database.DB import emailDb
import poplib

Result = Tools.Result
Dumps = Tools.dumps
Error = Tools.errors


class Email:
    def __init__(self, email, content, is_auth=False, sender=None, content_type='html'):
        self.SenderEmail = sender
        self.IsHtml = content_type
        self.Email = email
        self.Created_at = datetime.now()
        self.Updated_at = datetime.now()
        self.Content = content
        self.IsAuth = is_auth

    @staticmethod
    def send_email_html(email, text):
        pass

    @staticmethod
    def send_email_text(email, text):
        pass
