from Classes.Email import Email
from Classes.Tools import Tools

Result = Tools.Result
Dumps = Tools.dumps

class EmailSender:
    @staticmethod
    def Send(email,text):
        state , des = Email(email,text,False).__add__()
        # send real_email
        return Result(state,des)
    @staticmethod
    def send_auth_email(email,text):
        state , des = Email(email,text,True).__add__()
        # send real_email
        return Result(state,"d" if state else des)
