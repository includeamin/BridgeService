from kavenegar import *
from Classes.Tools import Tools
from Classes.SMS import Sms

Error = Tools.errors
Result = Tools.Result
Dumps = Tools.dumps


# '6F43714C41496A2B386F624931324F61494A323157736F6A73394B766B504871'
class Kavenegar:
    @staticmethod
    def send(phonenumber, text):
        try:
            # api = KavenegarAPI('6F43714C41496A2B386F624931324F61494A323157736F6A73394B766B504871')
            # params = {'sender': '1000596446', 'receptor': '09374397796"', 'message' : ".وب سرویس پیام کوتاه کاوه نگار"}
            # response = api.sms_send(params)
            # print(response)

            return Result(True, "D")
        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def check_recieve(phonenumber):
        try:
            return Result(True, "R")
        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def send_authenticate(phonenumber, code):
        try:

            url = f'https://api.kavenegar.com/v1/6F43714C41496A2B386F624931324F61494A323157736F6A73394B766B504871/verify/lookup.json?receptor={phonenumber}&token={code}&template=chichi-ath'
            response = requests.get(url)
            a = Sms(phonenumber, code, True, panel_response=response.json()).__add__()

            return Result(True, Dumps({"Code": code,
                                       "PhoneNumber": phonenumber,
                                       "Result": a
                                       }))
        except Exception as ex:
            return Result(False, ex.args)
