from datetime import datetime
from Classes.Tools import Tools
from Database.DB import smsDb

Result = Tools.Result
Error = Tools.errors
Dumps = Tools.dumps


class Sms:
    def __init__(self, phonenumber: str, text: str, isauth=False, panel_response=None):
        self.PhoneNumber = phonenumber
        self.Create_at = datetime.now()
        self.Text: str = text
        self.IsAuth: bool = isauth
        self.PanelResponse = panel_response

    def __add__(self):
        try:
            smsDb.insert_one(self.__dict__)
            return Result(True, "D")
        except Exception as ex:
            return Result(False, ex.args)
