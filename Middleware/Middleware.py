from flask import request
from functools import wraps
from Classes.Tools import Tools
import requests

Result = Tools.Result
Error = Tools.errors


def validate_request(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # validate access
        return f(*args, **kwargs)

    return decorated_function


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        return f(*args, **kwargs)

    return decorated_function


def json_body_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # validate access
        if request.get_json() is None:
            return Result(False, Error("JBR"))
        return f(*args, **kwargs)

    return decorated_function


def form_body_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # validate access
        if request.form is None:
            return Result(False, Error("FBR"))
        return f(*args, **kwargs)

    return decorated_function
