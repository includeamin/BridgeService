from flask import Blueprint, request
from Classes.Email_Send import EmailSender
from Middleware.Middleware import form_body_required

Email_Api = Blueprint("Email_Api", __name__, "template")


@Email_Api.route('/email/send', methods=["POST"])
@form_body_required
def send_email():
    return EmailSender.Send(request.form["Email"], request.form["Text"])


@Email_Api.route('/email/authentication/send', methods=["POST"])
@form_body_required
def send_email_atuh():
    return EmailSender.send_auth_email(request.form["Email"], request.form["Code"])
