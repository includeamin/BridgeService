from flask import Blueprint, request
from Classes.Kavenegar_Sms import Kavenegar

SMS = Blueprint('SMS', __name__, template_folder='template')


@SMS.route('/sms/authenticate/send/<phonenumber>/<code>')
def send_authenticate_sms(phonenumber, code):
    return Kavenegar.send_authenticate(phonenumber, code)


@SMS.route('/sms/send', methods=["POST"])
def send_sms():
    return Kavenegar.send(phonenumber=request.form["PhoneNumber"], text=request.form["Text"])


@SMS.route('/sms/delivery/<phonenumber>')
def delivery_check(phonenumber):
    return Kavenegar.check_recieve(phonenumber=phonenumber)
