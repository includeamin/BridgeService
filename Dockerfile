#FROM pypy:3.6
#
#EXPOSE 3008
#
#RUN apt-get update -y && \
#    apt-get install -y python3 python3-dev python3-pip
#
#
#
## We copy just the requirements.txt first to leverage Docker cache
#COPY ./requirements.txt ./app/requirements.txt
#
#COPY . ./app
#
#
#WORKDIR /app
#
#
#RUN pip3  install -r requirements.txt
#RUN pip3 install gunicorn
#RUN pip3 install termcolor
#
#
#
#
#
#
##CMD ["python3","-u","app.py"]
#CMD ["/usr/local/bin/gunicorn","--certfile","cert.crt","--keyfile","key.key", "--config", "gunicorn_config.py" , "app:app"]

FROM includeamin/ubuntu_fastapi:v2

EXPOSE 3008
COPY . app
WORKDIR app
RUN ls
RUN pip3 install kavenegar

#CMD ["gunicorn" , "app:app","-w","2", "-k", "uvicorn.workers.UvicornWorker","--bind","0.0.0.0:3008","--log-level","debug","--threads=2"]
CMD ["gunicorn","--config", "gunicorn_config.py" , "app:app"]
